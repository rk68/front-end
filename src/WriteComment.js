import React from 'react'
import TextareaAutosize from '@mui/base/TextareaAutosize';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';

const WriteComment = () => {
    return (
        <div style={{marginTop:30}}>
            <h5><b>POST A COMMENT</b></h5>
            <Typography variant='string' style={{marginTop:100}}>Logged in as Harsh Patel.{'    '}</Typography>
            <Button>Logout?</Button>
            <br />
            <form>
                <TextareaAutosize
                    maxRows={1}
                    aria-label="maximum height"
                    placeholder="Write Comment here" 
                    style={{width:"100%",height:120}}
                />

                <Button variant="contained" sx={{ borderRadius:3,width:250,mb:12,mt:1 }}>Post Comment</Button>
            </form>
        </div>
    )
}

export default WriteComment
