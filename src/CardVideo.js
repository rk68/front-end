import React from 'react'
import Card from "@mui/material/Card";
import CardMedia from "@mui/material/CardMedia";
import './CardVideo.css'

const CardVideo = (props) => {
    return (
        <div>
           <Card className='videocard'sx={{borderRadius: 4,bgcolor:'#FAF9F8'}}>
                <CardMedia className='card'
                
                    component="video"
                    height="180"
                    controls
                    src={props.src1}
                    frameborder="0"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                    alt="green iguana"
                /> 
                </Card>
        </div>
    )
}

export default CardVideo
