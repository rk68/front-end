import React,{useState} from 'react'
import { Box, Grid, InputBase} from '@material-ui/core'
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';


import FormControlLabel from '@mui/material/FormControlLabel';
import FormControl from '@mui/material/FormControl';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';
import Radio from '@mui/material/Radio';
import RadioGroup from '@mui/material/RadioGroup';
import Button from '@mui/material/Button';

import './Signup.css'
import female from './image/Female.png'
import male from './image/Male.png'


const Singup = () => {
    const [Date1, setDate] = useState(null);
    const [Gender, setGender] = useState('male')
    const initialvalues={FirstName:"",LastName:"",Email:""}
    const [formValues, setformValues] = useState(initialvalues);
    const [formerror, setformerror] = useState({})
    const [issubmit, setissubmit] = useState(false)

    const handelChage=(e)=>{
        const {name,value} = e.target;
        setformValues({...formValues,[name]:value})
        // console.log(formValues);
    }

    const handelSubmit=(e)=>{
        
        e.preventDefault();
        console.log("hi");
        // console.log(`${formValues.FirstName}`);
        setformerror(validate(formValues));
        setissubmit(true);

    }

    const validate = (values) =>{
        const errors ={};
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
        const regex1 = /\d/;
        if(!values.FirstName){
            errors.FirstName="First Name is required"
        }else if(regex1.test(values.FirstName)){
            errors.FirstName="Enter valid First name"
        }

        if(!values.LastName){
            errors.LastName="Last Name is required"
        }else if(regex1.test(values.LastName)){
            errors.LastName="Enter valid Last name"
        }

        if(!values.Email){
            errors.Email="Email is required"
        }else if(!regex.test(values.Email)){
            errors.Email="Enter valid email"
        }
        return errors;
    }

    let t1 =`${Gender}`
    return (
        
        <div >  

            <Grid container  spacing={2}>
                <Grid item md={6}>
                    
                <Box sx={{mt:"15%",pr:4,Width: 300,height:"auto"}} className='image'>
                {t1==='female' ? <img src={female}  alt="male" style={{height:350}} /> : <img src={male} className='i1' alt="male" style={{height:350}} />  }
                    </Box>
                </Grid>

                <Grid item md={6}>

                    <Box sx={{bgcolor:"white",width:"95%",mt:"15%"}} className='setvalue'>
                        <form>
                            <Grid container direction="row">
                                    <Grid item xs={12}>
                                        <label className='titel'>Bodh Labs</label>
                                    </Grid>

                                    <Grid item xs={12} >
                                        <label className='sub-titel'>CREATE PROFILE</label>
                                    </Grid>


                                <Grid item xs={6} md={6} align='left' style={{marginTop:"40px",paddingLeft:"30px"}} >
                                    <label id='f-name' className='entry'>FirstName</label><br/>
                                    <InputBase value={formValues.FirstName} name='FirstName' onChange={handelChage} placeholder="Enter First Name" id="f-input" className='input-filed' variant="standard" required />
                                    <p id="error"> {formerror.FirstName} </p>
                                </Grid>

                                    <Grid item xs={6} md={6} align='left' style={{marginTop:"40px"}} >
                                          <label id='l-name' className='entry'>Last Name</label><br />
                                          <InputBase  value={formValues.LastName}  name='LastName' onChange={handelChage} placeholder="Enter Last Name" id="l-input" className='input-filed' variant="standard" required />
                                          <p id="error"> {formerror.LastName} </p>
                                </Grid>

                                <Grid item xs={12} align='left' style={{marginTop:"20px",paddingLeft:"30px"}}>
                                    <label id='email' className='entry'>Email</label><br />
                                    <InputBase value={formValues.Email} name='Email' style={{width:'90%'}} onChange={handelChage} placeholder="Enter Email id" id="e-input" className='input-filed' variant="standard" required />
                                    <p id="error"> {formerror.Email} </p>
                                </Grid>

                                <Grid item xs={12} align='left' style={{marginTop:"20px",paddingLeft:"30px"}}>
                                    <label id='dob' className='entry'>DOB</label><br />
                                    
                                    <LocalizationProvider  dateAdapter={AdapterDateFns} required  >
                                        <DatePicker 
                                            inputFormat="dd/MM/yyyy"
                                            value={Date1}
                                            onChange={(newValue) => {
                                                setDate(newValue);
                                            }}
                                            maxDate={"02-01-2020"}
                                            renderInput={(params) => <TextField variant="standard"  {...params} />}
                                        />
                                    </LocalizationProvider>
                                </Grid>

                                <Grid item xs={12} align='left' style={{marginTop:"20px",paddingLeft:"30px"}}>
                                    <label id='gender' className='entry'>Gender</label><br />
                                    <FormControl component="fieldset">
                                        <RadioGroup row aria-label="gender" name="row-radio-buttons-group" value={Gender} onChange={(e)=>setGender(e.target.value)} required >
                                            <FormControlLabel id='male' value="male"  control={<Radio />} label="Male" />
                                            <FormControlLabel id='female' value="female" control={<Radio />} label="Female" />
                                        </RadioGroup>
                                    </FormControl>
                                </Grid>

                                <Grid item  xs={12} align='center' style={{marginTop:"18px",marginBottom:'14px'}}>
                                    <Button id='b1' variant="contained" onClick={handelSubmit}>SUBMIT</Button>
                                </Grid>

                            </Grid>
                            {/* </FormControl> */}
                        </form>
                    </Box>
                    
                </Grid>

            </Grid>
        </div>
    )
}

export default Singup
