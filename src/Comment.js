import React from 'react'
// import Comment1 from './Comment1';
import {Avatar, Grid, Paper } from "@material-ui/core";
import Typography from '@mui/material/Typography';

import imgLink from './image/Male.png'

const Comment = () => {

  const s ='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean luctus ut est sed faucibus. Duis bibendum ac ex vehicula laoreet.Suspendisse congue vulputate lobortis. Pellentesque at interdum tortor.'

  const data = [
    { imgLink:`${imgLink}`, name: "Rutvik Kansara",time:"16 December,2021",string:`${s}` },
    { imgLink:`${imgLink}`, name: "Rutvik Kansara",time:"16 December,2021",string:`${s}` },
    { imgLink:`${imgLink}`, name: "Rutvik Kansara",time:"16 December,2021",string:`${s}` },
    { imgLink:`${imgLink}`, name: "Rutvik Kansara",time:"16 December,2021",string:`${s}` },
    { imgLink:`${imgLink}`, name: "Rutvik Kansara",time:"16 December,2021",string:`${s}` },
    { imgLink:`${imgLink}`, name: "Rutvik Kansara",time:"16 December,2021",string:`${s}` }
  ]

    return (
        <div>
          
          {data.map((user) => (
            <Paper style={{ padding: "10px 20px", marginTop: 10,marginBottom:20,width:"90%" }}>
            <Grid container wrap="nowrap" spacing={5}>
              <Grid item>
                <Avatar  alt="Remy Sharp" src={user.imgLink} />
              </Grid>
              <Grid justifyContent="left" item xs zeroMinWidth>
                <Grid container justify="space-between">  
                    <Typography variant='h6' inline align="left" style={{ margin: 0, textAlign: "left",paddingBottom:3}}>{user.name}</Typography>
                    <Typography inline variant="subtitle2" align="right" style={{ textAlign: "left", color: "gray" }}>on {user.time}`</Typography>
                </Grid>
                <p style={{ textAlign: "left",color:"black"}}>
                  {user.string} 
                </p>
              </Grid>
            </Grid>
          </Paper>
           ))}
        </div>
    )
}

export default Comment
