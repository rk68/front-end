import React,{useState} from 'react'
import { Box, Grid} from '@material-ui/core'
import './Detail_blog.css'
import CardVideo from './CardVideo'
import CardImage from './CardImage'
import Typography from '@mui/material/Typography';
import image1 from './image/Rectangle14.png'
import Fab from "@material-ui/core/Fab";
import BookmarkBorderOutlinedIcon from '@mui/icons-material/BookmarkBorderOutlined';
import ThumbUpOutlinedIcon from '@mui/icons-material/ThumbUpOutlined';
import RemoveRedEyeOutlinedIcon from '@mui/icons-material/RemoveRedEyeOutlined';
import ForumOutlinedIcon from '@mui/icons-material/ForumOutlined';
import FacebookOutlinedIcon from '@mui/icons-material/FacebookOutlined';
import TwitterIcon from '@mui/icons-material/Twitter';
import WhatsappOutlinedIcon from '@mui/icons-material/WhatsappOutlined';
import TelegramIcon from '@mui/icons-material/Telegram';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import Card from '@mui/material/Card';
import image2 from './image/image1.png'

import Comment from './Comment'
import WriteComment from './WriteComment'

import './Detail_blog.css'

const s = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean luctus ut est sed faucibus. Duis bibendum ac ex vehicula laoreet.Suspendisse congue vulputate lobortis. Pellentesque at interdum tortor.'

const Detail_blog = () => {

    const [activity, setactivity] = useState("Vcomment")
    const [btncomment, setbtncomment] = useState("#04386C")
    const [btnwcomment, setbtnwcomment] = useState("#A9A8A8")

    console.log(`${btncomment}`);
    console.log(`${activity}`);
    return (
        <div>
            <Grid container justify="center" style={{backgroundColor:"white"}}>
            <Grid item md={8} xs={12} justify="center">
                <Box mt={3} ml={0.5} mr={3}>
                    
                    <Grid container justify="space-between">  
                        <Typography inline variant="body1" align="left" style={{color:"black"}}>Home/Defence</Typography>
                        <Typography inline variant="body1" align="right" style={{color:"black"}}>16 December, 2021</Typography>
                    </Grid>

                    <Box mt={3} ml={5}>

                        <Fab variant="extended"
                            size="medium"
                            color="primary"
                            aria-label="Add"
                            style={{width:"160px",height:35,backgroundColor:"#0787CB",fontSize:15}}
                            sx={{bgcolor:''}}
                        >
                            5 min Read
                        </Fab>
            
                        <Fab
                        sx={{ml:12}}
                          variant="extended"
                          size="medium"
                          color="primary"
                          aria-label="Add"
                          style={{marginLeft:5,width:"160px",height:35,backgroundColor:"#F5A26D",fontSize:15}}
                          // className={classes.margin}
                        >
                          Tranding One
                        </Fab>

                        <Box mt={4}>
                            <Grid container justify="space-between">
                                <Grid item xs={10}>
                                    
                                <Typography id='title'><b>Fasninated Unheard Story of IAF’s The Knights Squadron</b></Typography>
                                
                                </Grid>
                                
                                <Grid item xs={2} >
                                    
                                <BookmarkBorderOutlinedIcon sx={{mt:2,mr:2}} />
                                
                                </Grid>
                            </Grid>
                        </Box>

                        <Box ml={1} mt={1}>
                            <Grid container justify="space-between">
                                <Grid md={6}>
                                    <ThumbUpOutlinedIcon inline fontSize="string"/>
                                    <Typography sx={{ml:0.5}} variant="caption"  gutterBottom inline>100</Typography>
                                    <RemoveRedEyeOutlinedIcon inline sx={{ml:1}} fontSize="string"/>
                                    <Typography sx={{ml:0.5}} variant="caption"  gutterBottom inline>100</Typography>
                                    <ForumOutlinedIcon inline sx={{ml:1}} fontSize="string"/>
                                    <Typography sx={{ml:0.5}} variant="caption"  gutterBottom inline>100</Typography>
                                </Grid>
                            
                            
                                <Grid md={6}>
                                    <FacebookOutlinedIcon style={{ fill: '#0E8CF3' }} fontSize="string" inline/>
                                    <TwitterIcon style={{ fill: '#689ABB' }} fontSize="string" inline sx={{ml:1}}/>
                                    <WhatsappOutlinedIcon style={{ fill: '#52CC60' }} fontSize="string" inline sx={{ml:1}} />
                                    <TelegramIcon style={{ fill: '#2CB0F3' }} fontSize="string" inline sx={{ml:1}} />
                                </Grid>
                            </Grid>
                        </Box>

                        <Card sx={{ borderRadius: 4,bgcolor:'#FAF9F8',mt:7,height:'auto' }} className="card1" >
                            <CardMedia className='card2'
                              component="img"
                              src={image2}
                              alt="green iguana"
                            />
                        </Card>

                        <Card sx={{mt:3,width:"100%",}} style={{fontSize:"150"}}>

                        <CardContent className='imagecard' >
                            <p style={{color:"black"}}>
                                The 163 metres long warship has a full load displacement of 7,400 tonnes and maximum speed of 30 knots. The overall indigenous content of the project is approximately 75 per cent.
                                Apart from several indigenous equipment in the Float and Move categories, the destroyer is also installed with major indigenous weapons 
                                which include: Medium Range Surface-to-Air Missiles (BEL, Bangalore), BrahMos Surface-to-Surface Missiles (BrahMos Aerospace, New Delhi), Indigenous Torpedo Tube Launchers (Larsen & Toubro, Mumbai), Anti-Submarine Indigenous Rocket Launchers (Larsen & Toubro, Mumbai), 
                                76mm Super Rapid Gun Mount (BHEL, Haridwar).

                                The 163 metres long warship has a full load displacement of 7,400 tonnes and maximum speed of 30 knots. The overall indigenous content of the project is approximately 75 per cent.
                                Apart from several indigenous equipment in the Float and Move categories, the destroyer is also installed with major indigenous weapons 
                                which include: Medium Range Surface-to-Air Missiles (BEL, Bangalore), BrahMos Surface-to-Surface Missiles (BrahMos Aerospace, New Delhi), Indigenous Torpedo Tube Launchers (Larsen & Toubro, Mumbai), Anti-Submarine Indigenous Rocket Launchers (Larsen & Toubro, Mumbai), 
                                76mm Super Rapid Gun Mount (BHEL, Haridwar).

                                The 163 metres long warship has a full load displacement of 7,400 tonnes and maximum speed of 30 knots. The overall indigenous content of the project is approximately 75 per cent.
                                Apart from several indigenous equipment in the Float and Move categories, the destroyer is also installed with major indigenous weapons 
                                which include: Medium Range Surface-to-Air Missiles (BEL, Bangalore), BrahMos Surface-to-Surface Missiles (BrahMos Aerospace, New Delhi), Indigenous Torpedo Tube Launchers (Larsen & Toubro, Mumbai), Anti-Submarine Indigenous Rocket Launchers (Larsen & Toubro, Mumbai), 
                                76mm Super Rapid Gun Mount (BHEL, Haridwar).

                                The 163 metres long warship has a full load displacement of 7,400 tonnes and maximum speed of 30 knots. The overall indigenous content of the project is approximately 75 per cent.
                                Apart from several indigenous equipment in the Float and Move categories, the destroyer is also installed with major indigenous weapons 
                                which include: Medium Range Surface-to-Air Missiles (BEL, Bangalore), BrahMos Surface-to-Surface Missiles (BrahMos Aerospace, New Delhi), Indigenous Torpedo Tube Launchers (Larsen & Toubro, Mumbai), Anti-Submarine Indigenous Rocket Launchers (Larsen & Toubro, Mumbai), 
                                76mm Super Rapid Gun Mount (BHEL, Haridwar).

                                The 163 metres long warship has a full load displacement of 7,400 tonnes and maximum speed of 30 knots. The overall indigenous content of the project is approximately 75 per cent.
                                Apart from several indigenous equipment in the Float and Move categories, the destroyer is also installed with major indigenous weapons 
                                which include: Medium Range Surface-to-Air Missiles (BEL, Bangalore), BrahMos Surface-to-Surface Missiles (BrahMos Aerospace, New Delhi), Indigenous Torpedo Tube Launchers (Larsen & Toubro, Mumbai), Anti-Submarine Indigenous Rocket Launchers (Larsen & Toubro, Mumbai), 
                                76mm Super Rapid Gun Mount (BHEL, Haridwar).

                                The 163 metres long warship has a full load displacement of 7,400 tonnes and maximum speed of 30 knots. The overall indigenous content of the project is approximately 75 per cent.
                                Apart from several indigenous equipment in the Float and Move categories, the destroyer is also installed with major indigenous weapons 
                                which include: Medium Range Surface-to-Air Missiles (BEL, Bangalore), BrahMos Surface-to-Surface Missiles (BrahMos Aerospace, New Delhi), Indigenous Torpedo Tube Launchers (Larsen & Toubro, Mumbai), Anti-Submarine Indigenous Rocket Launchers (Larsen & Toubro, Mumbai), 
                                76mm Super Rapid Gun Mount (BHEL, Haridwar).

                                The 163 metres long warship has a full load displacement of 7,400 tonnes and maximum speed of 30 knots. The overall indigenous content of the project is approximately 75 per cent.
                                Apart from several indigenous equipment in the Float and Move categories, the destroyer is also installed with major indigenous weapons 
                                which include: Medium Range Surface-to-Air Missiles (BEL, Bangalore), BrahMos Surface-to-Surface Missiles (BrahMos Aerospace, New Delhi), Indigenous Torpedo Tube Launchers (Larsen & Toubro, Mumbai), Anti-Submarine Indigenous Rocket Launchers (Larsen & Toubro, Mumbai), 
                                76mm Super Rapid Gun Mount (BHEL, Haridwar).
                            </p>
                        </CardContent></Card>

                        {/* <pre> */}
                        {/* The 163 metres long warship has a full load displacement of 7,400 tonnes and maximum speed of 30 knots. The overall indigenous content of the project is approximately 75 per cent. */}
                        {/* Apart from several indigenous equipment in the Float and Move categories, the destroyer is also installed with major indigenous weapons which include: Medium Range Surface-to-Air Missiles (BEL, Bangalore), BrahMos Surface-to-Surface Missiles (BrahMos Aerospace, New Delhi), Indigenous Torpedo Tube Launchers (Larsen & Toubro, Mumbai), Anti-Submarine Indigenous Rocket Launchers (Larsen & Toubro, Mumbai), 76mm Super Rapid Gun Mount (BHEL, Haridwar). */}
                        {/* </pre> */}

                        <Box sx={{mt:5,mb:3}}>

                        <Fab variant="extended"
                            size="medium"
                            color="primary"
                            aria-label="Add"
                            style={{width:"160px",height:35,backgroundColor:`${btncomment}`,fontSize:15}}
                            onClick={(e)=>{setactivity("Vcomment");setbtncomment("#04386C");setbtnwcomment("#A9A8A8")}}
                        >
                            View Comment
                        </Fab>
            
                        <Fab
                        sx={{ml:12}}
                          variant="extended"
                          size="medium"
                          color="primary"
                          aria-label="Add"
                          style={{marginLeft:5,width:"160px",height:35,backgroundColor:`${btnwcomment}`,fontSize:15}}
                          onClick={(e)=>{setactivity("Wcomment");setbtncomment("#A9A8A8");setbtnwcomment("#04386C")}}
                          // className={classes.margin}
                        >
                          Post Comment
                        </Fab>
                        </Box>
                        {activity === "Vcomment" && <Comment />}
                        {activity === "Wcomment" && <WriteComment />}
                        

                        
                            
                        {/* <Comment /> */}
                        {/* <Comment /> */}
                        {/* <Comment /> */}
                        {/* <Comment /> */}
                        
                    </Box>
                </Box>
            </Grid>

            <Grid item md={3} xs={12} justify="center">
                
                <Box mt={3} ml={2} mr={1}>
                    <p style={{color:"black"}}><b >Related Video</b></p>
                    <CardVideo src1={"./Video/10.mp4"}/>
                    <CardVideo src1={"./Video/10.mp4"}/>
                </Box>
                <Box mt={4} ml={2} mr={1}>
                    <p style={{color:"black"}}><b>Recommended Blogs</b></p>
                    <CardImage image1={image1} title="Fasninated Unheard Story of IAF’s The Knights Squadron" date="29th November, 2021" />
                    <CardImage image1={image1} title="Fasninated Unheard Story of IAF’s The Knights Squadron" date="29th November, 2021" />
                    <CardImage image1={image1} title="Fasninated Unheard Story of IAF’s The Knights Squadron" date="29th November, 2021" />
                </Box>
            </Grid>
            </Grid>
            
        </div>
    )
}

export default Detail_blog
