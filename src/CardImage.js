import React from 'react'
import { Box, Grid} from '@material-ui/core'
import CardMedia from '@mui/material/CardMedia';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import CardContent from '@mui/material/CardContent';
import WatchLaterOutlinedIcon from '@mui/icons-material/WatchLaterOutlined';


const CardImage = (props) => {
    return (
        <div>
            <Card sx={{ display: 'flex',borderRadius: 4,bgcolor:'#FAF9F8',mt:2 }} className="card1" >
              <CardMedia className='card2'
                component="img"
                src={props.image1}
                alt="green iguana"
              />
              <CardContent className='imagecard'>
                <Typography variant="body2">
                <b style={{color:"black"}}>{props.title}</b>
                </Typography>
                
                <WatchLaterOutlinedIcon fontSize="string" sx={{mt:0}} />
                <Typography variant="string"  sx={{ml:0.5,fontSize:14}}>{props.date}</Typography>
                {/* <Box align="bottom">u</Box> */}
              </CardContent>
              
              
            </Card>
        </div>
    )
}

export default CardImage
